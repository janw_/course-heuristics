import java.util.Comparator;

public class Tile implements Comparator<Tile> {

    private int width  = 0,
                height = 0,
                xcoord = 0,
                ycoord = 0,
                category = 0;


    public void setWidth(int i) {
        width = i;
    }

    public int getWidth() {
        return width;
    }

    public void setHeight(int i) {
        height = i;
    }

    public int getHeight() {
        return height;
    }

    public int getArea() {
        return width*height;
    }

    public int getXCoord() {
        return xcoord;
    }

    public void setXCoord(int i) {
        xcoord = i;
    }

    public int getYCoord() {
        return ycoord;
    }

    public void setYCoord(int i) {
        ycoord = i;
    }

    public void flip() {
        int temp = width;
        width = height;
        height = temp;
    }

    public int compare(Tile t1, Tile t2) {
        return t1.getArea() - t2.getArea();
    }

}
